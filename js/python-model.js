// CONSTANTS
const modelPath = 'models/python-model/saved-model/model1303.json';
var model = {};

// LOAD MODEL AND CREATE NEURAL NETWORK
async function loadPythonModel() {

    // load model from file
    await $.getJSON(modelPath, function (json) {
        model = json
    });

    // create neural network
    createNN();
}

// CREATE NEURAL NETWORK
function createNN() {

    // variables
    var biases = [];
    var weights = [];

    // get biases
    model['bias'].forEach(element1 => {

        // variables
        layer = [];

        // loop through every layer
        element1.forEach(element2 => {

            // get neuron value
            value = [element2[0]];

            // add it to layer
            layer.push(value)
        });

        // add layer to biases
        biases.push(layer)
    });

    // weights
    model['weights'].forEach(element1 => {

        // variables
        layer = [];

        // loop through every layer
        element1.forEach(element2 => {

            // access row of weights
            value = [element2];

            // add row to layer
            layer.push(value)
        });

        // add layer to weights
        weights.push(layer)
    });

    // create model
    model = { 'biases': biases, 'weights': weights };
}

// PREDICTIONS
async function makePythonModelPrediction(image) {

    // initialize model
    await loadPythonModel();

    // variables
    var activation = 0;
    var previousLayerActivation = image;
    var nextLayerActivation = [];
    var tmp = 0;

    // loop through every layer
    for (layer = 0; layer < model['weights'].length; layer++) {

        // first multiply every weight by the respective neuron
        // loop through every neuron in a layer (part 1)
        model['weights'][layer].forEach(neuron1 => {

            // loop through every neuron in a layer (part 2)
            neuron1.forEach(neuron2 => {

                // loop through every weight and activation
                for (i = 0; i < neuron2.length; i++) {
                    activation = activation + neuron2[i] * previousLayerActivation[i];
                }

                // add every neuron activation value
                nextLayerActivation.push(activation);
                activation = 0;
            });
        });

        // add the bias and calculate the activation value
        for (i = 0; i < model['biases'][layer].length; i++) {
            tmp = nextLayerActivation[i] + model['biases'][layer][i][0];
            nextLayerActivation[i] = sigmoid(tmp);
        }


        // prepare next cycle
        previousLayerActivation = nextLayerActivation;
        nextLayerActivation = [];
    }

    // return index of max value
    return findMax(previousLayerActivation);

}

// SIGMOID FUNCTION
function sigmoid(z) {
    return 1 / (1 + Math.exp(-z));
}

// FIND MAX VALUE IN ARRAY
function findMax(array) {

    // variables
    maxIndex = 0;
    maxValue = array[0];

    // loop through array
    // find max value
    // set max index
    for (i = 1; i < array.length; i++) {
        if (maxValue < array[i]) {
            maxValue = array[i];
            maxIndex = i;
        }
    }

    // return max index
    return maxIndex;
}
