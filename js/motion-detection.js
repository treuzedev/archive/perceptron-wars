// VARIABLES
const CANVAS_BACKGROUD_COLOR = '#000000';
const LINE_COLOUR = '#FFFFFF';
const LINE_WIDTH = 15;
var context;
var canvas;
var rect;
var previousX = 0;
var previousY = 0;
var currentX = 0;
var currentY = 0;
var isPainting = false;

// PREPARE CANVAS
function prepareCanvas() {

    // get reference to the canvas
    // use context of that canvas to manipulate properties
    // 2d canvas
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    rect = canvas.getBoundingClientRect();

    // canvas properties
    context.fillStyle = CANVAS_BACKGROUD_COLOR;
    context.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight);
    context.strokeStyle = LINE_COLOUR;
    context.lineWidth = LINE_WIDTH;
    context.lineJoin = 'round';

    // detect touches and draw line
    detectTouch();
    touchScreenDrawing();
};

// DETECT TOUCH
function detectTouch() {

    // mouse down event listener
    document.addEventListener('mousedown', function (event) {

        // allow painting
        isPainting = true;

        //  set starting coordinates
        currentX = event.clientX - canvas.offsetLeft;
        currentY = event.clientY - rect.top;

        // drawline
        draw();
    });

    // mouseup event listener
    document.addEventListener('mouseup', function (event) {
        isPainting = false;
    });
}

// DRAW A LINE
function draw() {

    // mouse move event listener
    document.addEventListener('mousemove', function (event) {

        // set new coordinates
        previousX = currentX;
        previousY = currentY;
        currentX = event.clientX - canvas.offsetLeft;
        currentY = event.clientY - rect.top;

        // draw line
        if (isPainting) {
            drawLine();
        }
    });
}

// CANVAS LINE DRAWING
function drawLine() {
    context.beginPath();
    context.moveTo(previousX, previousY);
    context.lineTo(currentX, currentY);
    context.closePath();
    context.stroke();
}

// CLEAR CANVAS
function clearCanvas() {

    // reset canvas and coordinates
    console.log('clearcanvas');
    previousX = 0;
    previousY = 0;
    currentX = 0;
    currentY = 0;
    context.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

// TOUCHSCREEN DRAWING
function touchScreenDrawing() {

    // below is the logic for touchscreen motion detection
    // is painting is true / is false depending on touch
    // the line is drawn while touch is being sensed
    canvas.addEventListener('touchstart', function (event) {
        isPainting = true;
        currentX = event.touches[0].clientX - canvas.offsetLeft;
        currentY = event.touches[0].clientY - canvas.offsetTop;

    });

    canvas.addEventListener('touchend', function (event) {
        isPainting = false;

    });

    canvas.addEventListener('touchcancel', function (event) {
        isPainting = false;

    });

    canvas.addEventListener('touchmove', function (event) {

        if (isPainting) {
            previousX = currentX;
            currentX = event.touches[0].clientX - canvas.offsetLeft;

            previousY = currentY;
            currentY = event.touches[0].clientY - canvas.offsetTop;

            drawLine();
        }
    });
}