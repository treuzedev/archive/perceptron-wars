// VARIABLES
var counter = 0;

// MAKE PREDICTIONS
async function makePredictions() {

    // ui changes
    var pythonPredictionSpan = document.getElementById('pythonPredictionSpan');
    var tfjsPredictionSpan = document.getElementById('tfjsPredictionSpan');

    if (counter == 0) {
        pythonPredictionSpan.textContent = "Thinking..";
        tfjsPredictionSpan.textContent = "Thinking..";
        counter++;
    }

    // get canvas image
    image = processCanvasImage();

    // python prediction
    pythonModelPrediction = await makePythonModelPrediction(image);

    // tfjs prediction
    tfjsModelPrediction = await makeTFJSPrediction(image);

    // ui changes
    pythonPredictionSpan.textContent = pythonModelPrediction;
    tfjsPredictionSpan.textContent = tfjsModelPrediction;
}

// PROCESS
function processCanvasImage() {

    // load image
    let image = cv.imread(canvas);

    // convert image to bw
    cv.cvtColor(image, image, cv.COLOR_RGBA2GRAY, 0);

    // contrast edges
    cv.threshold(image, image, 175, 255, cv.THRESH_BINARY);

    // get drawing contours
    let contours = new cv.MatVector();
    let hierarchy = new cv.Mat();
    cv.findContours(image, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);

    // get bounding rectangle and crop the image
    let rectangle = cv.boundingRect(contours.get(0));
    image = image.roi(rectangle);

    // resize image
    // first calculate new dimension according to biggest size
    let height = image.rows;
    let width = image.cols;
    let ratio = 0;

    if (height > width) {
        height = 20;
        ratio = image.rows / height;
        width = Math.round(width / ratio);
    }

    else {
        width = 20;
        ratio = image.cols / width;
        height = Math.round(height / ratio);
    }

    let newSize = new cv.Size(width, height);

    cv.resize(image, image, newSize, 0, 0, cv.INTER_AREA);

    // add padding to all sides
    // make sure size is not bigger than 28
    let left = Math.ceil((28 - width) / 2);
    let right = Math.floor((28 - width) / 2);
    let bottom = Math.ceil((28 - height) / 2);
    let top = Math.floor((28 - height) / 2);

    let scalar = new cv.Scalar(0, 0, 0, 0);
    border = cv.BORDER_CONSTANT;

    cv.copyMakeBorder(image, image, top, bottom, left, right, border, scalar);

    // center of mass
    // get contours of the new image
    //  get the mean of the mass in the x and then in the y axis
    // m00 -> the are occupied by the digits
    // xMass and yMass corresponds to the coordinates of the center of mass
    cv.findContours(image, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
    const moments = cv.moments(contours.get(0), false);
    const xMass = moments.m10 / moments.m00;
    const yMass = moments.m01 / moments.m00;

    // shift the image
    // the center of mass needs to be at the center of the image
    // then use a function to the the work
    const xShift = Math.round(image.cols / 2.0 - xMass);
    const yShift = Math.round(image.rows / 2.0 - yMass);

    newSize = new cv.Size(image.cols, image.rows);
    const transformationMatrix = cv.matFromArray(2, 3, cv.CV_64FC1, [1, 0, xShift, 0, 1, yShift]);
    cv.warpAffine(image, image, transformationMatrix, newSize, cv.INTER_LINEAR, border, scalar);

    // image as values
    // transform from int to float
    // normalize values
    let pixelValues = image.data;
    pixelValues = Float32Array.from(pixelValues);
    pixelValues = pixelValues.map(function (item) {
        return item / 255.0;
    });

    // delete objects from memory
    image.delete();
    contours.delete();
    hierarchy.delete();
    transformationMatrix.delete();

    // return image as array
    return pixelValues;
}