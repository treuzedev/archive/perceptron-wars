# Perceptron Wars

## A crude comparison between a Raw Python Perceptron and a Tensorflow Perceptron

<br>

### Study Introduction

The aim of this project consisted is building two models with the same general architecture and compare their accuracy in predicting digits drawn on a HTML Canvas after being trained with the MNIST Digits.

<br>

### Results

Although both models share the same general architecture, i.e., both of them are Perceptron Neural Networks, they are actually quite a few differences between them. 
Among the most important differences, I would like to point out that the Raw Python Model is built with only one hidden layer of 30 units and the Tensorflow Model is built with 512 units on a first hidden layer and 64 units on a second hidden layer; furthermore, the Raw Python Model has a regularization technique applied when updating the weights and biases and the Tensorflow Model has a Dropout Layer Technique applied to the first hidden layer. The Raw Python Model was also trained for 100 epochs and the Tensorflow Model was trained for only 50 epochs.
Another big difference between the models is that the Raw Python Model uses a Sigmoid Activation Function whereas the Tensorflow Model used a ReLU Activation Function.
Despite these differences, their ability to predict human drawn digits on a HTML Canvas seems to be very acceptable (link to the webpage below).
Finally, I would like to summarize the results for both models on the Training, Validation and Test Data Sets, respectively, on the final epoch of training:

* ACCURACY
  * Raw Python Model -> 91,76% / 91,75% / 92,04%
  * Tensorflow Model -> 99.9% / 86,91% / 86,56%
  
  
* COST
  * Raw Python Model -> 0.58 / 0.58 / 0.57
  * Tensorflow Model -> 1.46 / 1.59 / 1.59
  
It is interesting to note that the Raw Python Model performs worse on the Training Data, compared to the Tensorflow Model, but has a better performance on the Validation and Test Data.
The cost function of the output layer of the Tensorflow Model is actually a Softmax Function, so it is natural that the cost values are different from the Raw Python Model.

<br>

### Website Preview

<img src="screenshots/screenshots1.png" height="300" width="450">
<br>
<img src="screenshots/screenshots2.png" height="300" width="450">

##### Webpage link to test models -> https://treuzedev.gitlab.io/perceptron-wars/

<br>

##### All data is archived in a .7z file, as the original files are very big and wouldn't otherwise be able to be pushed into Gitlab.
##### The .py file containing the Raw Python Model was moved from the notebooks folder to the models/python-model folder for better organization; if you plan to use the 3-python-model.ipynb, you need to move the model.py file into the notebooks folder or change the import statement.

##### This was done to better understand both Udemy's Complete 2020 Data Science & Machine Learning Bootcamp, Sections 9, 10, 11 and 12 and Michael Nielsen's book (http://neuralnetworksanddeeplearning.com/).

##### Use nbviwer, pasting this repo's notebook link, if Gitlab servers are down;
##### nbviwer website -> https://nbviewer.jupyter.org/
